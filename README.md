# VCMP Sample Plugin

A blank plugin sample.

```
vcmp_sample_plugin
├── README.md
├── src
│   ├── main.cpp
│   └── main.h
├── vendor
│   └── vcmp
│       └── VCMP.h
└── xmake.lua
```

## Prerequisties
* cpp compiler of your choice
* [xmake](https://xmake.io/#/guide/installation)

## Usage

### Linux

* Clone the repository
```
$  git clone https://gitea.com/disasterloop/vcmp_sample_plugin.git
```
* Change directory and build project
```
$ cd vcmp_sample_plugin && xmake
```
* Copy the plugin to the plugins directory of your VCMP Server
```
$ cp vcmp_sample_plugin/build/linux/x86_64/release/libvcmp_sample_plugin.so <VCMP_SERVER_PATH>/plugins
```
* Update the server.cfg

## License

Copyright (c) 2023 disasterloop

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
