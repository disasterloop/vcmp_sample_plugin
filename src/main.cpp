#include "main.h"

uint8_t OnServerInitialise() {
    std::cout << "Hello World!\n";
    return 1;
}

uint32_t VcmpPluginInit(PluginFuncs* pluginFunctions, PluginCallbacks* pluginCallbacks, PluginInfo* pluginInfo)
{
    pluginInfo->pluginVersion = 0x2137;
	pluginInfo->apiMajorVersion = PLUGIN_API_MAJOR;
	pluginInfo->apiMinorVersion = PLUGIN_API_MINOR;

    pluginCallbacks->OnServerInitialise = OnServerInitialise;

    return 1;
}
