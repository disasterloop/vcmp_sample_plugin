add_rules("mode.debug", "mode.release")

target("vcmp_sample_plugin")
    set_kind("shared")
    add_includedirs("vendor")
    add_files("src/main.cpp")
